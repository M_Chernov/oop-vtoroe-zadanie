package com.company.interfaces;

public interface GroupCommand {
    int highFive();
    int badGuys();
    void personalProgress();
}
