package com.company.interfaces;

public interface InputAndParser {
    void inputCommand();
    void commandParser(String parser);
}
