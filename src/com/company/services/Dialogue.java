package com.company.services;

import com.company.interfaces.InputAndParser;

import java.util.Scanner;

public class Dialogue implements InputAndParser {
    Group group = new Group();
    String commands ="averageScore - cредний балл всех студентов\n" +
            "personalProgress - средний балл каждого студента\n" +
            "highFive - число отличников\n" +
            "badGuys - число студентов, имеющих “неудовлетворительно”\n" +
            "";

    @Override
    public void inputCommand() {
            group.addStudents();
            System.out.println("Введите команду");
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            commandParser(command);
    }

    @Override
    public void commandParser(String parser) {
        switch (parser) {
            case "averageScore":
                group.inputCommand();
                break;
            case "personalProgress":
                group.personalProgress();
                break;
            case "highFive":
                System.out.println(group.highFive());
                break;
            case "badGuys" :
                System.out.println(group.badGuys());
                break;
            case "help":
                help();
                break;
            default:
                System.out.println("некорректная команда");
        }
        inputCommand();
    }

    void help() {
        System.out.println(commands);
    }
}
