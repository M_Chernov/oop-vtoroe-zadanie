package com.company.services;

public class Student extends StudentProgress {
    String FirstName, SecondName;

    public Student (String firstName, String secondName, int math, int chem, int phys) {
        this.FirstName = firstName;
        this.SecondName = secondName;
        this.Math = math;
        this.Chem = chem;
        this.Phys = phys;
    }
}
