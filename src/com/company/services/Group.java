package com.company.services;

import com.company.interfaces.GroupCommand;
import com.company.interfaces.InputAndParser;

import java.util.Scanner;

public class Group implements InputAndParser, GroupCommand {
    Student[] students = new Student[5];

    public void addStudents() {
        students[0] = new Student("Иванов", "Иван", 5, 5, 5);
        students[1] = new Student("Семен", "Семенов", 3, 2, 3);
        students[2] = new Student("Олег", "Олегов", 4, 4, 4);
        students[3] = new Student("Василий", "Васильев", 3, 3, 4);
        students[4] = new Student("Александр", "Невский", 2, 5, 3);
    }

    @Override
    public void inputCommand() {
        System.out.println("Введите название предмета");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        commandParser(command);
    }

    @Override
    public void commandParser(String parser) {
        int a = 0;
        switch (parser) {
            case "math":
                for (int i = 0; i < students.length; i++) {
                    a += students[i].Math;
                }
                System.out.println((double) a / students.length);
                break;
            case "chem":
                for (int i = 0; i < students.length; i++) {
                    a += students[i].Chem;
                }
                System.out.println((double) a / students.length);
                break;
            case "phys":
                for (int i = 0; i < students.length; i++) {
                    a += students[i].Phys;
                }
                System.out.println((double) a / students.length);
                break;
            default:
                System.out.println("некорректная команда");
            case"back":
                return;
        }
        inputCommand();
    }

    @Override
    public int highFive() {
        int firstMark = 0;
        int secondMark = 0;
        int thirdMark = 0;
        int five = 0;
        for (int i = 0; i < students.length; i++) {
            firstMark = students[i].Math;
            secondMark = students[i].Chem;
            thirdMark = students[i].Phys;
            if (firstMark == 5 && secondMark == 5 && thirdMark == 5)
                five++;
        }
        return five;
    }

    @Override
    public int badGuys() {
        int firstMark = 0;
        int secondMark = 0;
        int thirdMark = 0;
        int bad = 0;
        for (int i = 0; i < students.length; i++) {
            firstMark = students[i].Math;
            secondMark = students[i].Chem;
            thirdMark = students[i].Phys;
            if (firstMark == 2 || secondMark == 2 || thirdMark == 2) {
                bad++;
            }
        }
        return bad;
    }

    @Override
    public void personalProgress() {
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].FirstName+ " " + students[i].SecondName + " " + students[i].personMark());
        }
    }
}
